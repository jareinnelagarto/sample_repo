<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    	$this->app->bind('App\Repositories\GaSeoToolRepositoryInterface', 'App\Repositories\GaSeoToolRepository');
    	$this->app->bind('App\Repositories\GaSeoToolCriteriaRepositoryInterface', 'App\Repositories\GaSeoToolCriteriaRepository');
    	$this->app->bind('App\Repositories\GaSeoToolCriteriaValueRepositoryInterface', 'App\Repositories\GaSeoToolCriteriaValueRepository');
    }
}
