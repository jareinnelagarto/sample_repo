<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GaHighSchoolStudyType extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tbhighschooltype';

    public static function getByID($id){
    	return self::where('highschooltypeID', '=' , $id);
    }

    public function getType(){
    	return $this->type;
    }
    
}
