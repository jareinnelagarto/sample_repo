<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GaAcadDegree extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tbacaddegree';

    public static function getByID($id){
    	return self::where('acaddegreeID', '=' , $id);
    }
    
}
