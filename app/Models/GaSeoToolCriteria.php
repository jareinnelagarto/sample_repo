<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GaSeoToolCriteria extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tblSeoToolCriteria';

    public function findOneBy($seoToolCriteriaID){

    	$query = $this->newQuery()
    				  ->where('id', $seoToolCriteriaID);

		return $query->first();
	}

}
