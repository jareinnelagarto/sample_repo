<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GaCountry extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tbcountry';
    


    public static function getByID($id){
    	return self::where('countryID', '=' , $id);
    }

}
