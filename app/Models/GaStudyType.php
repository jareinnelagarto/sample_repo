<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GaStudyType extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tbstudytype';

    public static function getByID($id){
    	return self::where('studytypeID', '=' , $id);
    }

    public function getType(){
    	return $this->studytype;
    }
    
    
}
