<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GaVolunteerType extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tbvolunteertype';

    public static function getByID($id){
    	return self::where('volunteertypeID', '=' , $id);
    }

    public function getType(){
    	return $this->volunteertype;
    }
    
    
}
