<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GaForeignLang extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tbforeignlang';


    public static function getByID($id){
    	return self::where('foreignlangID', '=' , $id);
    }

    public function getCourse(){
    	return $this->foreignlang;
    }
    
}
