<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GaScholarshipType extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tbScholarshipType';

    public static function getByID($id){
    	return self::where('scholarshiptypeid', '=' , $id);
    }

    public function getType(){
    	return $this->type;
    }
    
    
}
