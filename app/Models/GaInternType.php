<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GaInternType extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tbinterntype';

    public static function getByID($id){
    	return self::where('interntypeID', '=' , $id);
    }

    public function getType(){
    	return $this->interntype;
    }
    
}
