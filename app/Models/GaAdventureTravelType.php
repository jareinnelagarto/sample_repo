<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GaAdventureTravelType extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tbadvtravtype';

    public static function getByID($id){
    	return self::where('advtravtypeID', '=' , $id);
    }

    public function getType(){
    	return $this->advtravtype;
    }
    
    
}
