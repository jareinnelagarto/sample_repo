<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GaMainRegion extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tbmainregion';
    
    public static function getByID($id){
    	return self::where('mainregionID', '=' , $id);
    }
}
