<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GaTerm extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tbterm';
    

    public static function getByID($id){
    	return self::where('termID', '=' , $id);
    }

    public function getTerm(){
    	return $this->term;
    }
    
}
