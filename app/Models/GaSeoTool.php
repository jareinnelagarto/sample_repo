<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GaSeoTool extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tblSeoTool';

    public function findOneBy($seoToolID){

    	$query = $this->newQuery()
    				  ->where('id', $seoToolID);

		return $query->first();
	}
}
