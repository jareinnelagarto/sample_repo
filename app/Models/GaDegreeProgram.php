<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GaDegreeProgram extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tbdegprog';

    public static function getByID($id){
    	return self::where('degprogID', '=' , $id);
    }

    public function getCourse(){
    	return $this->degprog;
    }
    
    
}
