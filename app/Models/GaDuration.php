<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GaDuration extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tbtimespan';
    

    public static function getByID($id){
    	return self::where('timespanID', '=' , $id);
    }

    public function getTerm(){
    	return $this->timespan;
    }
}
