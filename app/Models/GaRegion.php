<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GaRegion extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tbregion';
    
    public static function getByID($id){
    	return self::where('regionID', '=' , $id);
    }
}
