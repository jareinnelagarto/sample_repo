<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GaSeoToolCriteriaValue extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tblSeoToolCriteriaValue';


    public function findOneBy($criteria){

    	$query = $this->newQuery();
		foreach ($criteria as $key => $value) {
			$query->where('criteriaID', $key)->where('value',$value);
		}

		return $query->first();
	}
}
