<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GaCity extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tbcity';
    
    public static function getByID($id){
    	return self::where('cityID', '=' , $id);
    }
}
