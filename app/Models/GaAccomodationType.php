<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GaAccomodationType extends Model
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tblAccomodationType';

    public static function getByID($id){
    	return self::where('ID', '=' , $id);
    }

    public function getType(){
    	return $this->accomType;
    }
    
}
