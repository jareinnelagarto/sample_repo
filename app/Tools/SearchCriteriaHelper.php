<?php

namespace App\Tools;

use App\Tools\Constants;

use App\Models\GaCity;
use App\Models\GaMainRegion;
use App\Models\GaProgramSearchCriteria;

use DB;

class SearchCriteriaHelper
{

	private static $instance;

	public function __construct(){
		self::$instance = $this;
	}

	public static function getInstance(){
		if(self::$instance === null){
			self::$instance = new self();
		}
		return self::$instance;
	}

	// public function searchWorldRegion($param){
	// 	return DB::table('tbregion')->where('tbregion.urlAlias', strtolower($param))->first(['*', DB::raw('16384 as criteriaID')]);
	// }

	// public function searchCountry($param){
	// 	return DB::table('tbcountry')->where('tbcountry.urlAlias', strtolower($param))->first(['*', DB::raw('2 as criteriaID')]);
	// }

	// public function searchMainRegion($param){
	// 	return DB::table('tbmainregion')->where('tbmainregion.urlAlias', strtolower($param))->first(['*', DB::raw('8 as criteriaID')]);
	// }

	// public function searchCity($param){
	// 	return DB::table('tbcity')->where('tbcity.urlAlias', strtolower($param))->first(['*', DB::raw('16 as criteriaID')]);
	// }

	public function searchCriteriaInDB($tableName, $urlAlias){
		return DB::table('tb'.$tableName)->where('tb'.$tableName.'.urlAlias', strtolower($urlAlias))->first();
	}

	public function arrangeCriteria($paramsList){

	    usort($paramsList, function ($a, $b){
	    	return $a['criteriaID'] >= $b['criteriaID'];
		});
		return $paramsList;
	}

	public function resolveValues($match, $params){
		$function = 'build'.$match;

		return $this->$function($params);
	}
	
	protected function buildPROGRAMCOUNT($params){

		$programID = $params[Constants::PROGRAMID];
		
		return 25;
	}

	protected function buildTYPE($params){
		$programID = $params[Constants::PROGRAMID];
		$gaType = Constants::TYPE_MAPPER[$programID];
		$typeID = $params[Constants::TYPE_MAPPER_CRITERIA[$programID]];

		$gaType = $gaType::getByID($typeID)->first();
		$name = Constants::CRITERIA_MAPPER_VALUE[Constants::TYPE_MAPPER_CRITERIA[$programID]];
		return $gaType->$name;
	}

	protected function buildTERM($params){
		$programID = $params[Constants::PROGRAMID];
		$gaTerm = Constants::TERM_MAPPER[$programID];
		$termID = $params[Constants::TERM_MAPPER_CRITERIA[$programID]];

		$gaTerm = $gaTerm::getByID($termID)->first();
		$name = Constants::CRITERIA_MAPPER_VALUE[Constants::TERM_MAPPER_CRITERIA[$programID]];
		return $gaTerm->$name;
		
	}

	protected function buildCOURSE($params){
		$programID = $params[Constants::PROGRAMID];
		$gaCourse = Constants::COURSE_MAPPER[$programID];
		$courseID = $params[Constants::COURSE_MAPPER_CRITERIA[$programID]];

		$gaCourse = $gaCourse::getByID($courseID)->first();
		$name = Constants::CRITERIA_MAPPER_VALUE[Constants::COURSE_MAPPER_CRITERIA[$programID]];
		return $gaCourse->$name;

	}

	protected function buildCITY($params){
		$gaCity = GaCity::where('cityID', $params[Constants::CITYID])->first();
		return $gaCity->city;
	}

	protected function buildMAINREGION($params){
		$gaMainRegion = GaMainRegion::where('mainregionID', $params[Constants::MAINREGIONID])->first();
		return $gaCountry->mainregion;
	}

	protected function buildLOCATION($params){
		$programID = $params[Constants::PROGRAMID];
		$result = GaProgramSearchCriteria::where('programID', $programID)->orderBy('priorityLevel','asc')->get();
		$location = [];
		foreach ($result as $item) {
			foreach ($params as $key => $value) {
				if($item->searchCriteriaID == $key && array_key_exists($key, Constants::LOCATION_MAPPER)){
					$temp = Constants::LOCATION_MAPPER[$key];
					$temp = $temp::getByID($value)->first();
					$name = Constants::CRITERIA_MAPPER_VALUE[$key];
					$location[$item->priorityLevel] = $temp->$name;
				}	
			}
		}
		return implode(",", $location);
	}
}