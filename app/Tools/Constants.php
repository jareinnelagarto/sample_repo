<?php

namespace App\Tools;

class Constants {
	
	const PROGRAMID = 1;
	const COUNTRYID = 2;
	const STATEID = 4;
	const MAINREGIONID = 8;
	const CITYID = 16;
	const ADVTRAVTYPEID = 32;
	const DEGPROGID = 64;
	const STUDYTYPEID = 128;
	const HIGHSCHOOLSTUDYTYPEID = 256;
	const DURATIONID = 512;
	const FOREIGNLANGID = 1024;
	const INTERNTYPEID = 2048;
	const TERMID = 4096;
	const VOLUNTEERTYPEID = 8192;
	const REGIONID = 16384;
	const ONLINETEFL = 32768;
	const ACCOMTYPEID = 65536;
	const ACADDEGREEID = 131072;
	const SCHOLARSHIPTYPEID = 262144;	

	const VA = 2;
	const TA = 3;
	const IA = 4;
	const JA = 5;
	const SA = 6;
	const LS = 7;
	const AT = 8;
	const HS = 9;
	const TEFL = 11;
	const DA = 12;


	const CRITERIA_MAPPER = [
		self::PROGRAMID => "App\Models\GaProgram",
		self::COUNTRYID => "App\Models\GaCountry",
		self::STATEID => "App\Models\GaState",
		self::MAINREGIONID => "App\Models\GaMainRegion",
		self::CITYID => "App\Models\GaCity",
		self::ADVTRAVTYPEID => "App\Models\GaAdventureTraveType",
		self::DEGPROGID => "App\Models\GaDegreeProgram",
		self::STUDYTYPEID => "App\Models\GaStudyType",
		self::HIGHSCHOOLSTUDYTYPEID => "App\Models\GaHighSchoolStudyType",
		self::DURATIONID => "App\Models\GaDuration",
		self::FOREIGNLANGID => "App\Models\GaForeignLang",
		self::INTERNTYPEID => "App\Models\GaInternType",
		self::TERMID => "App\Models\GaTerm",
		self::VOLUNTEERTYPEID => "App\Models\GaVolunteerType",
		self::REGIONID => "App\Models\GaRegion",
		self::ACCOMTYPEID => "App\Models\GaAccomodationType",
		self::ACADDEGREEID => "App\Models\GaAcadDegree",
		self::SCHOLARSHIPTYPEID => "App\Models\GaScholarshipType"
	];

	const CRITERIA_MAPPER_VALUE = [
		self::PROGRAMID => "program",
		self::COUNTRYID => "country",
		self::STATEID => "state",
		self::MAINREGIONID => "mainregion",
		self::CITYID => "city",
		self::ADVTRAVTYPEID => "advtravtype",
		self::DEGPROGID => "degprog",
		self::STUDYTYPEID => "studyType",
		self::HIGHSCHOOLSTUDYTYPEID => "type",
		self::DURATIONID => "timespan",
		self::FOREIGNLANGID => "foreignlang",
		self::INTERNTYPEID => "interntype",
		self::TERMID => "term",
		self::VOLUNTEERTYPEID => "volunteertype",
		self::REGIONID => "region",
		self::ACCOMTYPEID => "accomType",
		self::ACADDEGREEID => "acaddegree",
		self::SCHOLARSHIPTYPEID => "type"
	];

	const TYPE_MAPPER = [
		self::VA => self::CRITERIA_MAPPER[self::VOLUNTEERTYPEID],
		self::IA => self::CRITERIA_MAPPER[self::INTERNTYPEID],
		self::JA => self::CRITERIA_MAPPER[self::INTERNTYPEID],
		self::AT => self::CRITERIA_MAPPER[self::ADVTRAVTYPEID],
		self::HS => self::CRITERIA_MAPPER[self::HIGHSCHOOLSTUDYTYPEID],
		self::DA => self::CRITERIA_MAPPER[self::STUDYTYPEID]
	];

	const TYPE_MAPPER_CRITERIA = [
		self::VA => self::VOLUNTEERTYPEID,
		self::IA => self::INTERNTYPEID,
		self::JA => self::INTERNTYPEID,
		self::AT => self::ADVTRAVTYPEID,
		self::HS => self::HIGHSCHOOLSTUDYTYPEID,
		self::DA => self::STUDYTYPEID
	];

	const TERM_MAPPER = [
		self::VA => self::CRITERIA_MAPPER[self::DURATIONID],
		self::SA => self::CRITERIA_MAPPER[self::TERMID], 
		self::HS => self::CRITERIA_MAPPER[self::DURATIONID]
	];

	const TERM_MAPPER_CRITERIA = [
		self::VA => self::DURATIONID,
		self::SA => self::TERMID,
		self::HS => self::DURATIONID
	];

	const COURSE_MAPPER = [
		self::SA => self::CRITERIA_MAPPER[self::DEGPROGID],
		self::LS => self::CRITERIA_MAPPER[self::FOREIGNLANGID],
		self::DA => self::CRITERIA_MAPPER[self::DEGPROGID]
	];

	const COURSE_MAPPER_CRITERIA = [
		self::SA => self::DEGPROGID,
		self::LS => self::FOREIGNLANGID,
		self::DA => self::DEGPROGID
	];

	const LOCATION_MAPPER = [
		self::CITYID => self::CRITERIA_MAPPER[self::CITYID],
		self::MAINREGIONID => self::CRITERIA_MAPPER[self::MAINREGIONID],
		self::COUNTRYID => self::CRITERIA_MAPPER[self::COUNTRYID],
		self::REGIONID => self::CRITERIA_MAPPER[self::REGIONID]
	];
}