<?php

namespace App\Tools;

use App\Tools\Constants;
use App\Tools\SearchCriteriaHelper;

class PageMetaHelper {


	private static $instance;

	public function __construct(){
		self::$instance = $this;
	}

	public static function getInstance(){
		if(self::$instance === null){
			self::$instance = new self();
		}
		return self::$instance;
	}

	public function fetchCorrectPageMetas($seoTool, $params){
		$title = $seoTool->title;
		$description = $seoTool->description;

		preg_match_all('/{[A-Za-z0-9_]*}/', $title, $titleMatches);
		preg_match_all('/{[A-Za-z0-9_]*}/', $description, $descriptionMatches);

		return ["title" => $this->replace($title, $titleMatches, $params), 
				"description" => $this->replace($description, $descriptionMatches, $params)];

	}

	protected function replace($original, $arrayMatches, $params){
		$replacement = "";
		$new = $original;
		foreach ($arrayMatches as $match) {
			foreach ($match as $m) {
				$replacement = $this->getReplacement($params, $m);
				$new = preg_replace('/'.$m.'/', $replacement, $new);			
			}
		}
		return $new;
	}

	protected function getReplacement($params, $match){

		$match = substr($match, 1, strlen($match) - 2);
		$match = strtoupper($match);

		$searchCriteriaHelper = SearchCriteriaHelper::getInstance();
		return $searchCriteriaHelper->resolveValues($match, $params);
	}
}