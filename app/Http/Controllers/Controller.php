<?php

namespace App\Http\Controllers;

use DB;

use Laravel\Lumen\Routing\Controller as BaseController;

use App\Tools\SearchCriteriaHelper;
use App\Tools\ParameterValidator;
use App\Tools\Constants;


class Controller extends BaseController
{

	protected $searchCriteriaHelper,
			  $parameterValidator;

	const CRITERIA = [
		Constants::WORLD_REGION => 'region',
		Constants::COUNTRY => 'country',
		Constants::MAIN_REGION => 'mainregion',
		Constants::CITY => 'city'
	];

	public function __construct(SearchCriteriaHelper $searchCriteriaHelper, ParameterValidator $parameterValidator){
		$this->searchCriteriaHelper = $searchCriteriaHelper;
		$this->parameterValidator = $parameterValidator;
	}

	// public function getURLCriteria($params = null){

	// 	$params = [
	// 		"study-abroad",
	// 		"asia",
	// 		"europe",
	// 		"luzon",
	// 		"visayas",
	// 		"tacloban-city",
	// 		"philippines",
	// 		"study-abroad-1"
	// 	];

	// 	$criteriaObjects = [];
	// 	foreach ($params as $urlAlias) {
	// 		foreach (self::CRITERIA as $criteriaID => $criterion) {
	// 			if($criteria = $this->searchCriteriaHelper->searchCriteriaInDB($criterion, $urlAlias)){
	// 				$criteria->criteriaID = $criteriaID;
	// 				$criteriaObjects[$urlAlias] = $criteria;
	// 				break;
	// 			}
	// 		}
	// 	}

	// 	foreach (self::CRITERIA as $criteriaID => $criterion) {

	// 		$tempArray = array_where($criteriaObjects, function($key, $value) use($criteriaID){
	// 			return $value->criteriaID == $criteriaID;
	// 		});

	// 		foreach ($tempArray as $key => $value) {

				
	// 		}

	// 	}
	// }

	public function getSample(){
		return response()->json(['name' => 'Abigail', 'state' => 'CA']);
	}


}
