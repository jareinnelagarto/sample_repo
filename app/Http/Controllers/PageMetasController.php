<?php

namespace App\Http\Controllers;

use DB;

use Laravel\Lumen\Routing\Controller as BaseController;

use App\Tools\SearchCriteriaHelper;
use App\Tools\ParameterValidator;
use App\Tools\Constants;
use App\Tools\PageMetaHelper;

use App\Repositories\GaSeoToolCriteriaValueRepository;
use App\Repositories\GaSeoToolCriteriaRepository;
use App\Repositories\GaSeoToolRepository;


class PageMetasController extends BaseController
{
	protected $searchCriteriaHelper;
	protected $gaSeoToolCriteriaValueRepository;
	protected $gaSeoToolCriteriaRepository;
	protected $gaSeoToolRepository;
	protected $pageMetaHelper;

	public function __construct(SearchCriteriaHelper $searchCriteriaHelper, GaSeoToolCriteriaValueRepository $gaSeoToolCriteriaValueRepository, GaSeoToolCriteriaRepository $gaSeoToolCriteriaRepository, GaSeoToolRepository $gaSeoToolRepository, PageMetaHelper $pageMetaHelper){

		$this->searchCriteriaHelper = $searchCriteriaHelper;
		$this->gaSeoToolCriteriaValueRepository = $gaSeoToolCriteriaValueRepository;
		$this->gaSeoToolCriteriaRepository = $gaSeoToolCriteriaRepository;
		$this->gaSeoToolRepository = $gaSeoToolRepository;
		$this->pageMetaHelper = $pageMetaHelper;

	}

	public function getPageMetas(){

		$params = ["1" => "6", "2" => "43", "16" => "580", "64" => "161", "4096" => "1"];
		
		if($seoTool = $this->gaSeoToolRepository->findFromCriteria($params)){
		}
		else {
			$seoTool = $this->gaSeoToolRepository->findDefault($params);
		}
		$correctPageMetas = $this->pageMetaHelper->fetchCorrectPageMetas($seoTool, $params);

		return response()->json($correctPageMetas);
		//
	}

}