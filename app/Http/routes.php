<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/
$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
    $api->get('/sample', 'App\Http\Controllers\Controller@getSample');
    $api->get('/page-metas', 'App\Http\Controllers\PageMetasController@getPageMetas');
    
 //    $api->post('oauth/access_token', function() {
	//     return Response::json(Authorizer::issueAccessToken());
	// });
});