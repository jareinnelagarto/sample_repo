<?php

namespace App\Repositories;

use App\Repositories\GaSeoToolRepositoryInterface;
use App\Models\GaSeoTool;
use App\Models\GaSeoToolCriteriaValue;

class GaSeoToolCriteriaValueRepository implements GaSeoToolCriteriaValueRepositoryInterface
{

	protected $gaSeoToolCriteriaValue;

	public function __construct(GaSeoToolCriteriaValue $gaSeoToolCriteriaValue){
		$this->gaSeoToolCriteriaValue = $gaSeoToolCriteriaValue;
	}

	public function findOneWhere($criteria = array("*")){
		$query = $this->gaSeoToolCriteriaValue->newQuery();
		foreach ($criteria as $key => $value) {
			$query->where('criteriaID', $key)->where('value',$value);
		}
		var_dump($query->toSql());
		return $query->first();
		
	}


}
