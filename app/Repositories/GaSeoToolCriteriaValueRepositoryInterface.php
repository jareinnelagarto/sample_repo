<?php 

namespace App\Repositories;

use App\Repositories\Contracts\RepositoryInterface;

interface GaSeoToolCriteriaValueRepositoryInterface extends RepositoryInterface {
}