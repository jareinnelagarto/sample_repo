<?php

namespace App\Repositories\Contracts;

interface RepositoryInterface {
	/**
     * Find data by multiple fields
     *
     * @param array $criteria
     * @return object
     */
	public function findOneWhere($criteria = array("*"));
}