<?php

namespace App\Repositories;

use App\Repositories\GaSeoToolRepositoryInterface;
use App\Models\GaSeoTool;
use App\Models\GaSeoToolCriteriaValue;

class GaSeoToolRepository implements GaSeoToolRepositoryInterface
{

	protected $gaSeoTool;

	public function __construct(GaSeoTool $gaSeoTool){
		$this->gaSeoTool = $gaSeoTool;
	}
	public function findOneWhere($criteria = array("*")){}

	public function findFromCriteria($criteria = array("*")){
		$query = $this->gaSeoTool->newQuery();
		$query
			->leftJoin('tblSeoToolCriteria', 'tblSeoTool.id', '=', 'tblSeoToolCriteria.seoToolID');

		foreach ($criteria as $key => $value) {
			$query
				->whereIn('tblSeoToolCriteria.id', function($query) use ($criteria, $key, $value){
				    $query
				        ->select('tblSeoToolCriteriaValue.seoToolCriteriaID')
				        ->from('tblSeoToolCriteriaValue')
			        	->where('tblSeoToolCriteriaValue.criteriaID', '=', $key)
			        	->where('tblSeoToolCriteriaValue.value', '=', $value);
			    });
		}
		$query
			->whereNotIn('tblSeoToolCriteria.id', function($query) {
				$query
					->select('tblSeoToolCriteriaValue.seoToolCriteriaID')
					->from('tblSeoToolCriteriaValue')
					->where('tblSeoToolCriteriaValue.value', '=', 0);
			});
		return $query->first();
	}

	public function findDefault($criteria = array("*")){

		$query = $this->gaSeoTool->newQuery();
		$query
			->leftJoin('tblSeoToolCriteria', 'tblSeoTool.id', '=', 'tblSeoToolCriteria.seoToolID');

		foreach ($criteria as $key => $value) {
			if(1 <> $key){
				$query
					->whereIn('tblSeoToolCriteria.id', function($query) use ($criteria, $key, $value){
					    $query
					        ->select('tblSeoToolCriteriaValue.seoToolCriteriaID')
					        ->from('tblSeoToolCriteriaValue')
				        	->where('tblSeoToolCriteriaValue.criteriaID', '=', $key)
				        	->where('tblSeoToolCriteriaValue.value', '=', 0);
				    });
			} else {
				$query
					->whereIn('tblSeoToolCriteria.id', function($query) use ($criteria, $key, $value){
					    $query
					        ->select('tblSeoToolCriteriaValue.seoToolCriteriaID')
					        ->from('tblSeoToolCriteriaValue')
				        	->where('tblSeoToolCriteriaValue.criteriaID', '=', $key)
				        	->where('tblSeoToolCriteriaValue.value', '=', $value);
				    });
			}
		}
		$query
			->whereIn('tblSeoToolCriteria.id', function($query) {
				$query
					->select('tblSeoToolCriteriaValue.seoToolCriteriaID')
					->from('tblSeoToolCriteriaValue')
					->where('tblSeoToolCriteriaValue.value', '=', 0);
			});
		return $query->first();

	}

}
