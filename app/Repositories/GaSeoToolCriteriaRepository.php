<?php

namespace App\Repositories;

use App\Repositories\GaSeoToolRepositoryInterface;
use App\Models\GaSeoTool;
use App\Models\GaSeoToolCriteriaValue;

class GaSeoToolCriteriaRepository implements GaSeoToolCriteriaRepositoryInterface
{

	protected $gaSeoTool;

	public function __construct(GaSeoTool $gaSeoTool){
		$this->gaSeoTool = $gaSeoTool;
	}

	public function findOneWhere($criteria = array("*")){
		$query = $this->gaSeoTool->newQuery();
		

		return $query->first();
	}


}
